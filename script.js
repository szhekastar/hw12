document.addEventListener("DOMContentLoaded", function() {
    const images = document.querySelectorAll(".image-show");
    const buttons = document.querySelector(".btn");
    let currentImg = 0;
    let timerId;
    function interval() {
        timerId = setInterval(function(){
            images[currentImg].classList.add("hidden");
            currentImg++;
            if (currentImg === images.length) {
                currentImg = 0;
            }
            images[currentImg].classList.remove("hidden");
        }, 3000);
    }
    buttons.addEventListener("click", function(event) {
        if(event.target.id === "pause") {
            clearInterval(timerId);
        } else if (event.target.id === "resume") {
            interval();
        }
    })
    interval();
})